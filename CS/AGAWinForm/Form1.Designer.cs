﻿namespace AGATester
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBoxPressureTapUpstream = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDownRefTemp = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownFlowingTemp = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.numericUpDownTubeDiameter = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownOrificeDiameter = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownViscosity = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownStaticPressure = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownDiffPressure = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownSpecificGravity = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.numericUpDownResult = new System.Windows.Forms.NumericUpDown();
            this.buttonStart = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.numericUpDownMethane = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownNitrogen = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownCarbonDioxide = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownEthane = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownPropane = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownWater = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownHydrogenSulfide = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownHydrogen = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownCarbonMonoxide = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownOxygen = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownIButane = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownNButane = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownIPentane = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownNPentane = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownNHexane = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownNHeptane = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownNOctane = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownNNonane = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownNDecane = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownHelium = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownArgon = new System.Windows.Forms.NumericUpDown();
            this.label34 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.numericUpDownRefPress = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBoxUseGasComp = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.numericUpDownDiffInH2O = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownStaticPSI = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownOrificeInch = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownPipeInch = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownTemperatureF = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownAtmosPressKPA = new System.Windows.Forms.NumericUpDown();
            this.label35 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.numericUpDownAtmosPressPSI = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownFlowingCompressibility = new System.Windows.Forms.NumericUpDown();
            this.label47 = new System.Windows.Forms.Label();
            this.numericUpDownBaseCompressibility = new System.Windows.Forms.NumericUpDown();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.numericUpDownBaseTempF = new System.Windows.Forms.NumericUpDown();
            this.label52 = new System.Windows.Forms.Label();
            this.numericUpDownOrificeTemperatureF = new System.Windows.Forms.NumericUpDown();
            this.label53 = new System.Windows.Forms.Label();
            this.numericUpDownOrificeTemperatureC = new System.Windows.Forms.NumericUpDown();
            this.label54 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.numericUpDownRefPressPSI = new System.Windows.Forms.NumericUpDown();
            this.label55 = new System.Windows.Forms.Label();
            this.numericUpDownPipeTemperatureF = new System.Windows.Forms.NumericUpDown();
            this.label56 = new System.Windows.Forms.Label();
            this.numericUpDownPipeTemperatureC = new System.Windows.Forms.NumericUpDown();
            this.label57 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label62 = new System.Windows.Forms.Label();
            this.comboBoxOrificeMaterial = new System.Windows.Forms.ComboBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.comboBoxPipeMaterial = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.numericUpDownFlowMCF = new System.Windows.Forms.NumericUpDown();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsDefaultSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRefTemp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFlowingTemp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTubeDiameter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownOrificeDiameter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownViscosity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStaticPressure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDiffPressure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpecificGravity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMethane)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNitrogen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCarbonDioxide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownEthane)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPropane)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownWater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHydrogenSulfide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHydrogen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCarbonMonoxide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownOxygen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIButane)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNButane)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIPentane)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNPentane)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNHexane)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNHeptane)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNOctane)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNNonane)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNDecane)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHelium)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownArgon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRefPress)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDiffInH2O)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStaticPSI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownOrificeInch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPipeInch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTemperatureF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAtmosPressKPA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAtmosPressPSI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFlowingCompressibility)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBaseCompressibility)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBaseTempF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownOrificeTemperatureF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownOrificeTemperatureC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRefPressPSI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPipeTemperatureF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPipeTemperatureC)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFlowMCF)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkBoxPressureTapUpstream
            // 
            this.checkBoxPressureTapUpstream.AutoSize = true;
            this.checkBoxPressureTapUpstream.Location = new System.Drawing.Point(15, 126);
            this.checkBoxPressureTapUpstream.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxPressureTapUpstream.Name = "checkBoxPressureTapUpstream";
            this.checkBoxPressureTapUpstream.Size = new System.Drawing.Size(181, 21);
            this.checkBoxPressureTapUpstream.TabIndex = 1;
            this.checkBoxPressureTapUpstream.Text = "Pressure Tap Upstream";
            this.checkBoxPressureTapUpstream.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 94);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Flowing Temp";
            // 
            // numericUpDownRefTemp
            // 
            this.numericUpDownRefTemp.DecimalPlaces = 6;
            this.numericUpDownRefTemp.Location = new System.Drawing.Point(185, 199);
            this.numericUpDownRefTemp.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownRefTemp.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownRefTemp.Minimum = new decimal(new int[] {
            5000,
            0,
            0,
            -2147483648});
            this.numericUpDownRefTemp.Name = "numericUpDownRefTemp";
            this.numericUpDownRefTemp.Size = new System.Drawing.Size(85, 22);
            this.numericUpDownRefTemp.TabIndex = 5;
            this.numericUpDownRefTemp.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericUpDownRefTemp.ValueChanged += new System.EventHandler(this.numericUpDownRefTemp_ValueChanged);
            // 
            // numericUpDownFlowingTemp
            // 
            this.numericUpDownFlowingTemp.DecimalPlaces = 2;
            this.numericUpDownFlowingTemp.Location = new System.Drawing.Point(136, 91);
            this.numericUpDownFlowingTemp.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownFlowingTemp.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownFlowingTemp.Minimum = new decimal(new int[] {
            20000,
            0,
            0,
            -2147483648});
            this.numericUpDownFlowingTemp.Name = "numericUpDownFlowingTemp";
            this.numericUpDownFlowingTemp.Size = new System.Drawing.Size(101, 22);
            this.numericUpDownFlowingTemp.TabIndex = 6;
            this.numericUpDownFlowingTemp.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownFlowingTemp.ValueChanged += new System.EventHandler(this.numericUpDownFlowingTemp_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 196);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Pipe Diameter";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 161);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Orifice Diameter";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(825, 500);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Viscosity";
            this.label5.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 30);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "Static Pressure";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 62);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 17);
            this.label7.TabIndex = 11;
            this.label7.Text = "Diff Pressure";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(27, 133);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(140, 17);
            this.label9.TabIndex = 13;
            this.label9.Text = "Ideal Specific Gravity";
            // 
            // numericUpDownTubeDiameter
            // 
            this.numericUpDownTubeDiameter.DecimalPlaces = 3;
            this.numericUpDownTubeDiameter.Location = new System.Drawing.Point(136, 193);
            this.numericUpDownTubeDiameter.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownTubeDiameter.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownTubeDiameter.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDownTubeDiameter.Name = "numericUpDownTubeDiameter";
            this.numericUpDownTubeDiameter.Size = new System.Drawing.Size(101, 22);
            this.numericUpDownTubeDiameter.TabIndex = 14;
            this.numericUpDownTubeDiameter.Value = new decimal(new int[] {
            10226,
            0,
            0,
            131072});
            this.numericUpDownTubeDiameter.ValueChanged += new System.EventHandler(this.numericUpDownTubeDiameter_ValueChanged);
            // 
            // numericUpDownOrificeDiameter
            // 
            this.numericUpDownOrificeDiameter.DecimalPlaces = 3;
            this.numericUpDownOrificeDiameter.Location = new System.Drawing.Point(136, 159);
            this.numericUpDownOrificeDiameter.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownOrificeDiameter.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownOrificeDiameter.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDownOrificeDiameter.Name = "numericUpDownOrificeDiameter";
            this.numericUpDownOrificeDiameter.Size = new System.Drawing.Size(101, 22);
            this.numericUpDownOrificeDiameter.TabIndex = 15;
            this.numericUpDownOrificeDiameter.Value = new decimal(new int[] {
            508,
            0,
            0,
            65536});
            this.numericUpDownOrificeDiameter.ValueChanged += new System.EventHandler(this.numericUpDownOrificeDiameter_ValueChanged);
            // 
            // numericUpDownViscosity
            // 
            this.numericUpDownViscosity.DecimalPlaces = 6;
            this.numericUpDownViscosity.Location = new System.Drawing.Point(932, 500);
            this.numericUpDownViscosity.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownViscosity.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownViscosity.Name = "numericUpDownViscosity";
            this.numericUpDownViscosity.Size = new System.Drawing.Size(160, 22);
            this.numericUpDownViscosity.TabIndex = 16;
            this.numericUpDownViscosity.Value = new decimal(new int[] {
            10268,
            0,
            0,
            393216});
            this.numericUpDownViscosity.Visible = false;
            // 
            // numericUpDownStaticPressure
            // 
            this.numericUpDownStaticPressure.DecimalPlaces = 3;
            this.numericUpDownStaticPressure.Location = new System.Drawing.Point(136, 27);
            this.numericUpDownStaticPressure.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownStaticPressure.Maximum = new decimal(new int[] {
            200000,
            0,
            0,
            0});
            this.numericUpDownStaticPressure.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDownStaticPressure.Name = "numericUpDownStaticPressure";
            this.numericUpDownStaticPressure.Size = new System.Drawing.Size(101, 22);
            this.numericUpDownStaticPressure.TabIndex = 17;
            this.numericUpDownStaticPressure.Value = new decimal(new int[] {
            4101324,
            0,
            0,
            196608});
            this.numericUpDownStaticPressure.ValueChanged += new System.EventHandler(this.numericUpDownStaticPressure_ValueChanged);
            // 
            // numericUpDownDiffPressure
            // 
            this.numericUpDownDiffPressure.DecimalPlaces = 4;
            this.numericUpDownDiffPressure.Location = new System.Drawing.Point(136, 59);
            this.numericUpDownDiffPressure.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownDiffPressure.Maximum = new decimal(new int[] {
            200000,
            0,
            0,
            0});
            this.numericUpDownDiffPressure.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDownDiffPressure.Name = "numericUpDownDiffPressure";
            this.numericUpDownDiffPressure.Size = new System.Drawing.Size(101, 22);
            this.numericUpDownDiffPressure.TabIndex = 18;
            this.numericUpDownDiffPressure.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownDiffPressure.ValueChanged += new System.EventHandler(this.numericUpDownDiffPressure_ValueChanged);
            // 
            // numericUpDownSpecificGravity
            // 
            this.numericUpDownSpecificGravity.DecimalPlaces = 6;
            this.numericUpDownSpecificGravity.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDownSpecificGravity.Location = new System.Drawing.Point(185, 130);
            this.numericUpDownSpecificGravity.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownSpecificGravity.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownSpecificGravity.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDownSpecificGravity.Name = "numericUpDownSpecificGravity";
            this.numericUpDownSpecificGravity.Size = new System.Drawing.Size(103, 22);
            this.numericUpDownSpecificGravity.TabIndex = 20;
            this.numericUpDownSpecificGravity.Value = new decimal(new int[] {
            607527,
            0,
            0,
            393216});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(235, 658);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 17);
            this.label10.TabIndex = 21;
            this.label10.Text = "Flow:";
            // 
            // numericUpDownResult
            // 
            this.numericUpDownResult.DecimalPlaces = 6;
            this.numericUpDownResult.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDownResult.Location = new System.Drawing.Point(285, 656);
            this.numericUpDownResult.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownResult.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.numericUpDownResult.Name = "numericUpDownResult";
            this.numericUpDownResult.ReadOnly = true;
            this.numericUpDownResult.Size = new System.Drawing.Size(96, 22);
            this.numericUpDownResult.TabIndex = 22;
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(56, 652);
            this.buttonStart.Margin = new System.Windows.Forms.Padding(4);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(152, 28);
            this.buttonStart.TabIndex = 23;
            this.buttonStart.Text = "Calculate Flow";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click_1);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(92, 23);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(79, 17);
            this.label13.TabIndex = 26;
            this.label13.Text = "Methane %";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(95, 55);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(78, 17);
            this.label14.TabIndex = 27;
            this.label14.Text = "Nitrogen %";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(56, 91);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(115, 17);
            this.label15.TabIndex = 28;
            this.label15.Text = "Cabon Dioxide %";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(103, 121);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(69, 17);
            this.label16.TabIndex = 29;
            this.label16.Text = "Ethane %";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(95, 151);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(78, 17);
            this.label17.TabIndex = 30;
            this.label17.Text = "Propane %";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(109, 183);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(62, 17);
            this.label18.TabIndex = 31;
            this.label18.Text = "Water %";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(40, 217);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(133, 17);
            this.label19.TabIndex = 32;
            this.label19.Text = "Hydrogen Sulfide %";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(87, 247);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(86, 17);
            this.label20.TabIndex = 33;
            this.label20.Text = "Hydrogen %";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(37, 278);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(134, 17);
            this.label21.TabIndex = 34;
            this.label21.Text = "Carbon Monoxide %";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(100, 311);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(72, 17);
            this.label22.TabIndex = 35;
            this.label22.Text = "Oxygen %";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(323, 23);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(77, 17);
            this.label23.TabIndex = 36;
            this.label23.Text = "i-Butane %";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(317, 55);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(82, 17);
            this.label24.TabIndex = 37;
            this.label24.Text = "n-Butane %";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(315, 91);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(85, 17);
            this.label25.TabIndex = 38;
            this.label25.Text = "i-Pentane %";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(309, 119);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(90, 17);
            this.label26.TabIndex = 39;
            this.label26.Text = "n-Pentane %";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(313, 153);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(85, 17);
            this.label27.TabIndex = 40;
            this.label27.Text = "n-Hexane %";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(308, 185);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(91, 17);
            this.label28.TabIndex = 41;
            this.label28.Text = "n-Heptane %";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(316, 217);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(83, 17);
            this.label29.TabIndex = 42;
            this.label29.Text = "n-Octane %";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(312, 249);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(87, 17);
            this.label30.TabIndex = 43;
            this.label30.Text = "n-Nonane %";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(312, 282);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(86, 17);
            this.label31.TabIndex = 44;
            this.label31.Text = "n-Decane %";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(332, 315);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(67, 17);
            this.label32.TabIndex = 45;
            this.label32.Text = "Helium %";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(312, 346);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(46, 17);
            this.label33.TabIndex = 46;
            this.label33.Text = "Argon";
            this.label33.Visible = false;
            // 
            // numericUpDownMethane
            // 
            this.numericUpDownMethane.DecimalPlaces = 4;
            this.numericUpDownMethane.Location = new System.Drawing.Point(180, 21);
            this.numericUpDownMethane.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownMethane.Name = "numericUpDownMethane";
            this.numericUpDownMethane.Size = new System.Drawing.Size(84, 22);
            this.numericUpDownMethane.TabIndex = 49;
            this.numericUpDownMethane.Value = new decimal(new int[] {
            906724,
            0,
            0,
            262144});
            // 
            // numericUpDownNitrogen
            // 
            this.numericUpDownNitrogen.DecimalPlaces = 4;
            this.numericUpDownNitrogen.Location = new System.Drawing.Point(180, 53);
            this.numericUpDownNitrogen.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownNitrogen.Name = "numericUpDownNitrogen";
            this.numericUpDownNitrogen.Size = new System.Drawing.Size(84, 22);
            this.numericUpDownNitrogen.TabIndex = 50;
            this.numericUpDownNitrogen.Value = new decimal(new int[] {
            31284,
            0,
            0,
            262144});
            // 
            // numericUpDownCarbonDioxide
            // 
            this.numericUpDownCarbonDioxide.DecimalPlaces = 4;
            this.numericUpDownCarbonDioxide.Location = new System.Drawing.Point(180, 85);
            this.numericUpDownCarbonDioxide.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownCarbonDioxide.Name = "numericUpDownCarbonDioxide";
            this.numericUpDownCarbonDioxide.Size = new System.Drawing.Size(84, 22);
            this.numericUpDownCarbonDioxide.TabIndex = 51;
            this.numericUpDownCarbonDioxide.Value = new decimal(new int[] {
            4676,
            0,
            0,
            262144});
            // 
            // numericUpDownEthane
            // 
            this.numericUpDownEthane.DecimalPlaces = 4;
            this.numericUpDownEthane.Location = new System.Drawing.Point(180, 117);
            this.numericUpDownEthane.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownEthane.Name = "numericUpDownEthane";
            this.numericUpDownEthane.Size = new System.Drawing.Size(84, 22);
            this.numericUpDownEthane.TabIndex = 52;
            this.numericUpDownEthane.Value = new decimal(new int[] {
            45279,
            0,
            0,
            262144});
            // 
            // numericUpDownPropane
            // 
            this.numericUpDownPropane.DecimalPlaces = 4;
            this.numericUpDownPropane.Location = new System.Drawing.Point(180, 149);
            this.numericUpDownPropane.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownPropane.Name = "numericUpDownPropane";
            this.numericUpDownPropane.Size = new System.Drawing.Size(84, 22);
            this.numericUpDownPropane.TabIndex = 53;
            this.numericUpDownPropane.Value = new decimal(new int[] {
            828,
            0,
            0,
            196608});
            // 
            // numericUpDownWater
            // 
            this.numericUpDownWater.DecimalPlaces = 4;
            this.numericUpDownWater.Location = new System.Drawing.Point(180, 181);
            this.numericUpDownWater.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownWater.Name = "numericUpDownWater";
            this.numericUpDownWater.Size = new System.Drawing.Size(84, 22);
            this.numericUpDownWater.TabIndex = 54;
            // 
            // numericUpDownHydrogenSulfide
            // 
            this.numericUpDownHydrogenSulfide.DecimalPlaces = 4;
            this.numericUpDownHydrogenSulfide.Location = new System.Drawing.Point(180, 213);
            this.numericUpDownHydrogenSulfide.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownHydrogenSulfide.Name = "numericUpDownHydrogenSulfide";
            this.numericUpDownHydrogenSulfide.Size = new System.Drawing.Size(84, 22);
            this.numericUpDownHydrogenSulfide.TabIndex = 55;
            // 
            // numericUpDownHydrogen
            // 
            this.numericUpDownHydrogen.DecimalPlaces = 4;
            this.numericUpDownHydrogen.Location = new System.Drawing.Point(180, 245);
            this.numericUpDownHydrogen.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownHydrogen.Name = "numericUpDownHydrogen";
            this.numericUpDownHydrogen.Size = new System.Drawing.Size(84, 22);
            this.numericUpDownHydrogen.TabIndex = 56;
            // 
            // numericUpDownCarbonMonoxide
            // 
            this.numericUpDownCarbonMonoxide.DecimalPlaces = 4;
            this.numericUpDownCarbonMonoxide.Location = new System.Drawing.Point(180, 276);
            this.numericUpDownCarbonMonoxide.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownCarbonMonoxide.Name = "numericUpDownCarbonMonoxide";
            this.numericUpDownCarbonMonoxide.Size = new System.Drawing.Size(84, 22);
            this.numericUpDownCarbonMonoxide.TabIndex = 57;
            // 
            // numericUpDownOxygen
            // 
            this.numericUpDownOxygen.DecimalPlaces = 4;
            this.numericUpDownOxygen.Location = new System.Drawing.Point(180, 308);
            this.numericUpDownOxygen.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownOxygen.Name = "numericUpDownOxygen";
            this.numericUpDownOxygen.Size = new System.Drawing.Size(84, 22);
            this.numericUpDownOxygen.TabIndex = 58;
            // 
            // numericUpDownIButane
            // 
            this.numericUpDownIButane.DecimalPlaces = 4;
            this.numericUpDownIButane.Location = new System.Drawing.Point(407, 21);
            this.numericUpDownIButane.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownIButane.Name = "numericUpDownIButane";
            this.numericUpDownIButane.Size = new System.Drawing.Size(84, 22);
            this.numericUpDownIButane.TabIndex = 59;
            this.numericUpDownIButane.Value = new decimal(new int[] {
            1037,
            0,
            0,
            262144});
            // 
            // numericUpDownNButane
            // 
            this.numericUpDownNButane.DecimalPlaces = 4;
            this.numericUpDownNButane.Location = new System.Drawing.Point(407, 53);
            this.numericUpDownNButane.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownNButane.Name = "numericUpDownNButane";
            this.numericUpDownNButane.Size = new System.Drawing.Size(84, 22);
            this.numericUpDownNButane.TabIndex = 60;
            this.numericUpDownNButane.Value = new decimal(new int[] {
            1563,
            0,
            0,
            262144});
            // 
            // numericUpDownIPentane
            // 
            this.numericUpDownIPentane.DecimalPlaces = 4;
            this.numericUpDownIPentane.Location = new System.Drawing.Point(407, 85);
            this.numericUpDownIPentane.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownIPentane.Name = "numericUpDownIPentane";
            this.numericUpDownIPentane.Size = new System.Drawing.Size(84, 22);
            this.numericUpDownIPentane.TabIndex = 61;
            this.numericUpDownIPentane.Value = new decimal(new int[] {
            321,
            0,
            0,
            262144});
            // 
            // numericUpDownNPentane
            // 
            this.numericUpDownNPentane.DecimalPlaces = 4;
            this.numericUpDownNPentane.Location = new System.Drawing.Point(407, 117);
            this.numericUpDownNPentane.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownNPentane.Name = "numericUpDownNPentane";
            this.numericUpDownNPentane.Size = new System.Drawing.Size(84, 22);
            this.numericUpDownNPentane.TabIndex = 62;
            this.numericUpDownNPentane.Value = new decimal(new int[] {
            443,
            0,
            0,
            262144});
            // 
            // numericUpDownNHexane
            // 
            this.numericUpDownNHexane.DecimalPlaces = 4;
            this.numericUpDownNHexane.Location = new System.Drawing.Point(407, 150);
            this.numericUpDownNHexane.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownNHexane.Name = "numericUpDownNHexane";
            this.numericUpDownNHexane.Size = new System.Drawing.Size(84, 22);
            this.numericUpDownNHexane.TabIndex = 63;
            this.numericUpDownNHexane.Value = new decimal(new int[] {
            393,
            0,
            0,
            262144});
            // 
            // numericUpDownNHeptane
            // 
            this.numericUpDownNHeptane.DecimalPlaces = 4;
            this.numericUpDownNHeptane.Location = new System.Drawing.Point(407, 182);
            this.numericUpDownNHeptane.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownNHeptane.Name = "numericUpDownNHeptane";
            this.numericUpDownNHeptane.Size = new System.Drawing.Size(84, 22);
            this.numericUpDownNHeptane.TabIndex = 64;
            // 
            // numericUpDownNOctane
            // 
            this.numericUpDownNOctane.DecimalPlaces = 4;
            this.numericUpDownNOctane.Location = new System.Drawing.Point(407, 214);
            this.numericUpDownNOctane.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownNOctane.Name = "numericUpDownNOctane";
            this.numericUpDownNOctane.Size = new System.Drawing.Size(84, 22);
            this.numericUpDownNOctane.TabIndex = 65;
            // 
            // numericUpDownNNonane
            // 
            this.numericUpDownNNonane.DecimalPlaces = 4;
            this.numericUpDownNNonane.Location = new System.Drawing.Point(407, 246);
            this.numericUpDownNNonane.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownNNonane.Name = "numericUpDownNNonane";
            this.numericUpDownNNonane.Size = new System.Drawing.Size(84, 22);
            this.numericUpDownNNonane.TabIndex = 66;
            // 
            // numericUpDownNDecane
            // 
            this.numericUpDownNDecane.DecimalPlaces = 4;
            this.numericUpDownNDecane.Location = new System.Drawing.Point(407, 278);
            this.numericUpDownNDecane.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownNDecane.Name = "numericUpDownNDecane";
            this.numericUpDownNDecane.Size = new System.Drawing.Size(84, 22);
            this.numericUpDownNDecane.TabIndex = 67;
            // 
            // numericUpDownHelium
            // 
            this.numericUpDownHelium.DecimalPlaces = 4;
            this.numericUpDownHelium.Location = new System.Drawing.Point(407, 310);
            this.numericUpDownHelium.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownHelium.Name = "numericUpDownHelium";
            this.numericUpDownHelium.Size = new System.Drawing.Size(84, 22);
            this.numericUpDownHelium.TabIndex = 68;
            // 
            // numericUpDownArgon
            // 
            this.numericUpDownArgon.DecimalPlaces = 6;
            this.numericUpDownArgon.Location = new System.Drawing.Point(370, 344);
            this.numericUpDownArgon.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownArgon.Name = "numericUpDownArgon";
            this.numericUpDownArgon.Size = new System.Drawing.Size(121, 22);
            this.numericUpDownArgon.TabIndex = 69;
            this.numericUpDownArgon.Visible = false;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(912, 452);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(48, 17);
            this.label34.TabIndex = 71;
            this.label34.Text = "Result";
            this.label34.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(27, 167);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(101, 17);
            this.label11.TabIndex = 72;
            this.label11.Text = "Base Pressure";
            // 
            // numericUpDownRefPress
            // 
            this.numericUpDownRefPress.DecimalPlaces = 3;
            this.numericUpDownRefPress.Location = new System.Drawing.Point(185, 165);
            this.numericUpDownRefPress.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownRefPress.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDownRefPress.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.numericUpDownRefPress.Name = "numericUpDownRefPress";
            this.numericUpDownRefPress.Size = new System.Drawing.Size(85, 22);
            this.numericUpDownRefPress.TabIndex = 73;
            this.numericUpDownRefPress.Value = new decimal(new int[] {
            101325,
            0,
            0,
            196608});
            this.numericUpDownRefPress.ValueChanged += new System.EventHandler(this.numericUpDownRefPress_ValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numericUpDownHelium);
            this.groupBox1.Controls.Add(this.numericUpDownNDecane);
            this.groupBox1.Controls.Add(this.numericUpDownNNonane);
            this.groupBox1.Controls.Add(this.numericUpDownNOctane);
            this.groupBox1.Controls.Add(this.numericUpDownNHeptane);
            this.groupBox1.Controls.Add(this.numericUpDownNHexane);
            this.groupBox1.Controls.Add(this.numericUpDownNPentane);
            this.groupBox1.Controls.Add(this.numericUpDownIPentane);
            this.groupBox1.Controls.Add(this.numericUpDownNButane);
            this.groupBox1.Controls.Add(this.label33);
            this.groupBox1.Controls.Add(this.numericUpDownArgon);
            this.groupBox1.Controls.Add(this.numericUpDownIButane);
            this.groupBox1.Controls.Add(this.numericUpDownOxygen);
            this.groupBox1.Controls.Add(this.numericUpDownCarbonMonoxide);
            this.groupBox1.Controls.Add(this.numericUpDownHydrogen);
            this.groupBox1.Controls.Add(this.numericUpDownHydrogenSulfide);
            this.groupBox1.Controls.Add(this.numericUpDownWater);
            this.groupBox1.Controls.Add(this.numericUpDownPropane);
            this.groupBox1.Controls.Add(this.numericUpDownEthane);
            this.groupBox1.Controls.Add(this.numericUpDownCarbonDioxide);
            this.groupBox1.Controls.Add(this.numericUpDownNitrogen);
            this.groupBox1.Controls.Add(this.numericUpDownMethane);
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Controls.Add(this.label31);
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Location = new System.Drawing.Point(572, 58);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(549, 372);
            this.groupBox1.TabIndex = 74;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Gas Composition";
            // 
            // checkBoxUseGasComp
            // 
            this.checkBoxUseGasComp.AutoSize = true;
            this.checkBoxUseGasComp.Location = new System.Drawing.Point(572, 30);
            this.checkBoxUseGasComp.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxUseGasComp.Name = "checkBoxUseGasComp";
            this.checkBoxUseGasComp.Size = new System.Drawing.Size(192, 21);
            this.checkBoxUseGasComp.TabIndex = 75;
            this.checkBoxUseGasComp.Text = "Enable Gas Compositions";
            this.checkBoxUseGasComp.UseVisualStyleBackColor = true;
            this.checkBoxUseGasComp.CheckedChanged += new System.EventHandler(this.checkBoxUseGasComp_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(245, 30);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 17);
            this.label12.TabIndex = 76;
            this.label12.Text = "Kpa";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(245, 62);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(33, 17);
            this.label36.TabIndex = 78;
            this.label36.Text = "Kpa";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(245, 94);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(17, 17);
            this.label37.TabIndex = 79;
            this.label37.Text = "C";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(245, 161);
            this.label38.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(30, 17);
            this.label38.TabIndex = 80;
            this.label38.Text = "mm";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(245, 196);
            this.label39.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(30, 17);
            this.label39.TabIndex = 81;
            this.label39.Text = "mm";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(417, 196);
            this.label40.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(34, 17);
            this.label40.TabIndex = 91;
            this.label40.Text = "Inch";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(417, 161);
            this.label41.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(34, 17);
            this.label41.TabIndex = 90;
            this.label41.Text = "Inch";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(417, 94);
            this.label42.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(16, 17);
            this.label42.TabIndex = 89;
            this.label42.Text = "F";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(439, 66);
            this.label43.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(42, 17);
            this.label43.TabIndex = 88;
            this.label43.Text = "\"H2O";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(417, 30);
            this.label44.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(27, 17);
            this.label44.TabIndex = 87;
            this.label44.Text = "Psi";
            // 
            // numericUpDownDiffInH2O
            // 
            this.numericUpDownDiffInH2O.DecimalPlaces = 4;
            this.numericUpDownDiffInH2O.Location = new System.Drawing.Point(308, 59);
            this.numericUpDownDiffInH2O.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownDiffInH2O.Maximum = new decimal(new int[] {
            200000,
            0,
            0,
            0});
            this.numericUpDownDiffInH2O.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDownDiffInH2O.Name = "numericUpDownDiffInH2O";
            this.numericUpDownDiffInH2O.Size = new System.Drawing.Size(101, 22);
            this.numericUpDownDiffInH2O.TabIndex = 86;
            this.numericUpDownDiffInH2O.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownDiffInH2O.ValueChanged += new System.EventHandler(this.numericUpDownDiffInH2O_ValueChanged);
            // 
            // numericUpDownStaticPSI
            // 
            this.numericUpDownStaticPSI.DecimalPlaces = 3;
            this.numericUpDownStaticPSI.Location = new System.Drawing.Point(308, 27);
            this.numericUpDownStaticPSI.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownStaticPSI.Maximum = new decimal(new int[] {
            200000,
            0,
            0,
            0});
            this.numericUpDownStaticPSI.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.numericUpDownStaticPSI.Name = "numericUpDownStaticPSI";
            this.numericUpDownStaticPSI.Size = new System.Drawing.Size(101, 22);
            this.numericUpDownStaticPSI.TabIndex = 85;
            this.numericUpDownStaticPSI.Value = new decimal(new int[] {
            4101324,
            0,
            0,
            196608});
            this.numericUpDownStaticPSI.ValueChanged += new System.EventHandler(this.numericUpDownStaticPSI_ValueChanged);
            // 
            // numericUpDownOrificeInch
            // 
            this.numericUpDownOrificeInch.DecimalPlaces = 3;
            this.numericUpDownOrificeInch.Location = new System.Drawing.Point(308, 159);
            this.numericUpDownOrificeInch.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownOrificeInch.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownOrificeInch.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDownOrificeInch.Name = "numericUpDownOrificeInch";
            this.numericUpDownOrificeInch.Size = new System.Drawing.Size(101, 22);
            this.numericUpDownOrificeInch.TabIndex = 84;
            this.numericUpDownOrificeInch.Value = new decimal(new int[] {
            508,
            0,
            0,
            65536});
            this.numericUpDownOrificeInch.ValueChanged += new System.EventHandler(this.numericUpDownOrificeInch_ValueChanged);
            // 
            // numericUpDownPipeInch
            // 
            this.numericUpDownPipeInch.DecimalPlaces = 3;
            this.numericUpDownPipeInch.Location = new System.Drawing.Point(308, 193);
            this.numericUpDownPipeInch.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownPipeInch.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownPipeInch.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDownPipeInch.Name = "numericUpDownPipeInch";
            this.numericUpDownPipeInch.Size = new System.Drawing.Size(101, 22);
            this.numericUpDownPipeInch.TabIndex = 83;
            this.numericUpDownPipeInch.Value = new decimal(new int[] {
            10226,
            0,
            0,
            131072});
            this.numericUpDownPipeInch.ValueChanged += new System.EventHandler(this.numericUpDownPipeInch_ValueChanged);
            // 
            // numericUpDownTemperatureF
            // 
            this.numericUpDownTemperatureF.DecimalPlaces = 2;
            this.numericUpDownTemperatureF.Location = new System.Drawing.Point(308, 91);
            this.numericUpDownTemperatureF.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownTemperatureF.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownTemperatureF.Minimum = new decimal(new int[] {
            5000,
            0,
            0,
            -2147483648});
            this.numericUpDownTemperatureF.Name = "numericUpDownTemperatureF";
            this.numericUpDownTemperatureF.Size = new System.Drawing.Size(101, 22);
            this.numericUpDownTemperatureF.TabIndex = 82;
            this.numericUpDownTemperatureF.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownTemperatureF.ValueChanged += new System.EventHandler(this.numericUpDownTemperatureF_ValueChanged);
            // 
            // numericUpDownAtmosPressKPA
            // 
            this.numericUpDownAtmosPressKPA.DecimalPlaces = 3;
            this.numericUpDownAtmosPressKPA.Location = new System.Drawing.Point(185, 23);
            this.numericUpDownAtmosPressKPA.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownAtmosPressKPA.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownAtmosPressKPA.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDownAtmosPressKPA.Name = "numericUpDownAtmosPressKPA";
            this.numericUpDownAtmosPressKPA.Size = new System.Drawing.Size(85, 22);
            this.numericUpDownAtmosPressKPA.TabIndex = 93;
            this.numericUpDownAtmosPressKPA.Value = new decimal(new int[] {
            9308,
            0,
            0,
            131072});
            this.numericUpDownAtmosPressKPA.ValueChanged += new System.EventHandler(this.numericUpDownAtmosPressKPA_ValueChanged);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(27, 26);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(147, 17);
            this.label35.TabIndex = 92;
            this.label35.Text = "Atmospheric Pressure";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(279, 26);
            this.label45.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(33, 17);
            this.label45.TabIndex = 94;
            this.label45.Text = "Kpa";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(419, 27);
            this.label46.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(27, 17);
            this.label46.TabIndex = 96;
            this.label46.Text = "Psi";
            // 
            // numericUpDownAtmosPressPSI
            // 
            this.numericUpDownAtmosPressPSI.DecimalPlaces = 3;
            this.numericUpDownAtmosPressPSI.Location = new System.Drawing.Point(320, 23);
            this.numericUpDownAtmosPressPSI.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownAtmosPressPSI.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.numericUpDownAtmosPressPSI.Name = "numericUpDownAtmosPressPSI";
            this.numericUpDownAtmosPressPSI.Size = new System.Drawing.Size(91, 22);
            this.numericUpDownAtmosPressPSI.TabIndex = 95;
            this.numericUpDownAtmosPressPSI.Value = new decimal(new int[] {
            13,
            0,
            0,
            65536});
            this.numericUpDownAtmosPressPSI.ValueChanged += new System.EventHandler(this.numericUpDownAtmosPressPSI_ValueChanged);
            // 
            // numericUpDownFlowingCompressibility
            // 
            this.numericUpDownFlowingCompressibility.DecimalPlaces = 4;
            this.numericUpDownFlowingCompressibility.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDownFlowingCompressibility.Location = new System.Drawing.Point(185, 60);
            this.numericUpDownFlowingCompressibility.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownFlowingCompressibility.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownFlowingCompressibility.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDownFlowingCompressibility.Name = "numericUpDownFlowingCompressibility";
            this.numericUpDownFlowingCompressibility.Size = new System.Drawing.Size(85, 22);
            this.numericUpDownFlowingCompressibility.TabIndex = 98;
            this.numericUpDownFlowingCompressibility.Value = new decimal(new int[] {
            13,
            0,
            0,
            65536});
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(27, 64);
            this.label47.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(158, 17);
            this.label47.TabIndex = 97;
            this.label47.Text = "Flowing Compressability";
            // 
            // numericUpDownBaseCompressibility
            // 
            this.numericUpDownBaseCompressibility.DecimalPlaces = 4;
            this.numericUpDownBaseCompressibility.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDownBaseCompressibility.Location = new System.Drawing.Point(185, 95);
            this.numericUpDownBaseCompressibility.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownBaseCompressibility.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDownBaseCompressibility.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDownBaseCompressibility.Name = "numericUpDownBaseCompressibility";
            this.numericUpDownBaseCompressibility.Size = new System.Drawing.Size(85, 22);
            this.numericUpDownBaseCompressibility.TabIndex = 100;
            this.numericUpDownBaseCompressibility.Value = new decimal(new int[] {
            13,
            0,
            0,
            65536});
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(27, 97);
            this.label48.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(143, 17);
            this.label48.TabIndex = 99;
            this.label48.Text = "Base Compressability";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(27, 207);
            this.label49.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(126, 17);
            this.label49.TabIndex = 101;
            this.label49.Text = "Base Temperature";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(273, 202);
            this.label50.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(17, 17);
            this.label50.TabIndex = 103;
            this.label50.Text = "C";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(413, 207);
            this.label51.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(16, 17);
            this.label51.TabIndex = 105;
            this.label51.Text = "F";
            // 
            // numericUpDownBaseTempF
            // 
            this.numericUpDownBaseTempF.DecimalPlaces = 4;
            this.numericUpDownBaseTempF.Location = new System.Drawing.Point(320, 201);
            this.numericUpDownBaseTempF.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownBaseTempF.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownBaseTempF.Minimum = new decimal(new int[] {
            5000,
            0,
            0,
            -2147483648});
            this.numericUpDownBaseTempF.Name = "numericUpDownBaseTempF";
            this.numericUpDownBaseTempF.Size = new System.Drawing.Size(85, 22);
            this.numericUpDownBaseTempF.TabIndex = 104;
            this.numericUpDownBaseTempF.Value = new decimal(new int[] {
            13,
            0,
            0,
            65536});
            this.numericUpDownBaseTempF.ValueChanged += new System.EventHandler(this.numericUpDownBaseTempF_ValueChanged);
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(413, 249);
            this.label52.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(16, 17);
            this.label52.TabIndex = 110;
            this.label52.Text = "F";
            // 
            // numericUpDownOrificeTemperatureF
            // 
            this.numericUpDownOrificeTemperatureF.DecimalPlaces = 4;
            this.numericUpDownOrificeTemperatureF.Location = new System.Drawing.Point(320, 240);
            this.numericUpDownOrificeTemperatureF.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownOrificeTemperatureF.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownOrificeTemperatureF.Minimum = new decimal(new int[] {
            5000,
            0,
            0,
            -2147483648});
            this.numericUpDownOrificeTemperatureF.Name = "numericUpDownOrificeTemperatureF";
            this.numericUpDownOrificeTemperatureF.Size = new System.Drawing.Size(85, 22);
            this.numericUpDownOrificeTemperatureF.TabIndex = 109;
            this.numericUpDownOrificeTemperatureF.Value = new decimal(new int[] {
            13,
            0,
            0,
            65536});
            this.numericUpDownOrificeTemperatureF.ValueChanged += new System.EventHandler(this.numericUpDownOrificeTemperatureF_ValueChanged);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(273, 244);
            this.label53.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(17, 17);
            this.label53.TabIndex = 108;
            this.label53.Text = "C";
            // 
            // numericUpDownOrificeTemperatureC
            // 
            this.numericUpDownOrificeTemperatureC.DecimalPlaces = 4;
            this.numericUpDownOrificeTemperatureC.Location = new System.Drawing.Point(185, 240);
            this.numericUpDownOrificeTemperatureC.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownOrificeTemperatureC.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownOrificeTemperatureC.Minimum = new decimal(new int[] {
            5000,
            0,
            0,
            -2147483648});
            this.numericUpDownOrificeTemperatureC.Name = "numericUpDownOrificeTemperatureC";
            this.numericUpDownOrificeTemperatureC.Size = new System.Drawing.Size(85, 22);
            this.numericUpDownOrificeTemperatureC.TabIndex = 107;
            this.numericUpDownOrificeTemperatureC.Value = new decimal(new int[] {
            13,
            0,
            0,
            65536});
            this.numericUpDownOrificeTemperatureC.ValueChanged += new System.EventHandler(this.numericUpDownOrificeTemperatureC_ValueChanged);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(27, 244);
            this.label54.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(135, 17);
            this.label54.TabIndex = 106;
            this.label54.Text = "Orifice Temperature";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(273, 171);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 17);
            this.label1.TabIndex = 111;
            this.label1.Text = "Kpa";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(409, 171);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 17);
            this.label8.TabIndex = 113;
            this.label8.Text = "Psi";
            // 
            // numericUpDownRefPressPSI
            // 
            this.numericUpDownRefPressPSI.DecimalPlaces = 3;
            this.numericUpDownRefPressPSI.Location = new System.Drawing.Point(316, 165);
            this.numericUpDownRefPressPSI.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownRefPressPSI.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDownRefPressPSI.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDownRefPressPSI.Name = "numericUpDownRefPressPSI";
            this.numericUpDownRefPressPSI.Size = new System.Drawing.Size(85, 22);
            this.numericUpDownRefPressPSI.TabIndex = 112;
            this.numericUpDownRefPressPSI.Value = new decimal(new int[] {
            103,
            0,
            0,
            0});
            this.numericUpDownRefPressPSI.ValueChanged += new System.EventHandler(this.numericUpDownRefPressPSI_ValueChanged);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(413, 308);
            this.label55.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(16, 17);
            this.label55.TabIndex = 118;
            this.label55.Text = "F";
            // 
            // numericUpDownPipeTemperatureF
            // 
            this.numericUpDownPipeTemperatureF.DecimalPlaces = 4;
            this.numericUpDownPipeTemperatureF.Location = new System.Drawing.Point(325, 305);
            this.numericUpDownPipeTemperatureF.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownPipeTemperatureF.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownPipeTemperatureF.Minimum = new decimal(new int[] {
            5000,
            0,
            0,
            -2147483648});
            this.numericUpDownPipeTemperatureF.Name = "numericUpDownPipeTemperatureF";
            this.numericUpDownPipeTemperatureF.Size = new System.Drawing.Size(85, 22);
            this.numericUpDownPipeTemperatureF.TabIndex = 117;
            this.numericUpDownPipeTemperatureF.Value = new decimal(new int[] {
            13,
            0,
            0,
            65536});
            this.numericUpDownPipeTemperatureF.ValueChanged += new System.EventHandler(this.numericUpDownPipeTemperatureF_ValueChanged);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(273, 308);
            this.label56.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(17, 17);
            this.label56.TabIndex = 116;
            this.label56.Text = "C";
            // 
            // numericUpDownPipeTemperatureC
            // 
            this.numericUpDownPipeTemperatureC.DecimalPlaces = 4;
            this.numericUpDownPipeTemperatureC.Location = new System.Drawing.Point(185, 305);
            this.numericUpDownPipeTemperatureC.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownPipeTemperatureC.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownPipeTemperatureC.Minimum = new decimal(new int[] {
            50000,
            0,
            0,
            -2147483648});
            this.numericUpDownPipeTemperatureC.Name = "numericUpDownPipeTemperatureC";
            this.numericUpDownPipeTemperatureC.Size = new System.Drawing.Size(85, 22);
            this.numericUpDownPipeTemperatureC.TabIndex = 115;
            this.numericUpDownPipeTemperatureC.Value = new decimal(new int[] {
            13,
            0,
            0,
            65536});
            this.numericUpDownPipeTemperatureC.ValueChanged += new System.EventHandler(this.numericUpDown2_ValueChanged);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(31, 308);
            this.label57.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(122, 17);
            this.label57.TabIndex = 114;
            this.label57.Text = "Pipe Temperature";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label62);
            this.groupBox2.Controls.Add(this.label40);
            this.groupBox2.Controls.Add(this.label41);
            this.groupBox2.Controls.Add(this.label42);
            this.groupBox2.Controls.Add(this.label44);
            this.groupBox2.Controls.Add(this.numericUpDownDiffInH2O);
            this.groupBox2.Controls.Add(this.numericUpDownStaticPSI);
            this.groupBox2.Controls.Add(this.numericUpDownOrificeInch);
            this.groupBox2.Controls.Add(this.numericUpDownPipeInch);
            this.groupBox2.Controls.Add(this.numericUpDownTemperatureF);
            this.groupBox2.Controls.Add(this.label39);
            this.groupBox2.Controls.Add(this.label38);
            this.groupBox2.Controls.Add(this.label37);
            this.groupBox2.Controls.Add(this.label36);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.numericUpDownDiffPressure);
            this.groupBox2.Controls.Add(this.numericUpDownStaticPressure);
            this.groupBox2.Controls.Add(this.numericUpDownOrificeDiameter);
            this.groupBox2.Controls.Add(this.numericUpDownTubeDiameter);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.numericUpDownFlowingTemp);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.checkBoxPressureTapUpstream);
            this.groupBox2.Location = new System.Drawing.Point(21, 30);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(461, 238);
            this.groupBox2.TabIndex = 119;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Basic Settings";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(413, 62);
            this.label62.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(42, 17);
            this.label62.TabIndex = 92;
            this.label62.Text = "\"H2O";
            this.label62.Click += new System.EventHandler(this.label62_Click);
            // 
            // comboBoxOrificeMaterial
            // 
            this.comboBoxOrificeMaterial.FormattingEnabled = true;
            this.comboBoxOrificeMaterial.Location = new System.Drawing.Point(185, 272);
            this.comboBoxOrificeMaterial.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxOrificeMaterial.Name = "comboBoxOrificeMaterial";
            this.comboBoxOrificeMaterial.Size = new System.Drawing.Size(151, 24);
            this.comboBoxOrificeMaterial.TabIndex = 120;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(29, 276);
            this.label58.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(103, 17);
            this.label58.TabIndex = 121;
            this.label58.Text = "Orifice Material";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(29, 341);
            this.label59.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(90, 17);
            this.label59.TabIndex = 123;
            this.label59.Text = "Pipe Material";
            // 
            // comboBoxPipeMaterial
            // 
            this.comboBoxPipeMaterial.FormattingEnabled = true;
            this.comboBoxPipeMaterial.Location = new System.Drawing.Point(185, 337);
            this.comboBoxPipeMaterial.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxPipeMaterial.Name = "comboBoxPipeMaterial";
            this.comboBoxPipeMaterial.Size = new System.Drawing.Size(151, 24);
            this.comboBoxPipeMaterial.TabIndex = 122;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label59);
            this.groupBox3.Controls.Add(this.comboBoxPipeMaterial);
            this.groupBox3.Controls.Add(this.label58);
            this.groupBox3.Controls.Add(this.comboBoxOrificeMaterial);
            this.groupBox3.Controls.Add(this.label55);
            this.groupBox3.Controls.Add(this.numericUpDownPipeTemperatureF);
            this.groupBox3.Controls.Add(this.label56);
            this.groupBox3.Controls.Add(this.numericUpDownPipeTemperatureC);
            this.groupBox3.Controls.Add(this.label57);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.numericUpDownRefPressPSI);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.label52);
            this.groupBox3.Controls.Add(this.numericUpDownOrificeTemperatureF);
            this.groupBox3.Controls.Add(this.label53);
            this.groupBox3.Controls.Add(this.numericUpDownOrificeTemperatureC);
            this.groupBox3.Controls.Add(this.label54);
            this.groupBox3.Controls.Add(this.label51);
            this.groupBox3.Controls.Add(this.numericUpDownBaseTempF);
            this.groupBox3.Controls.Add(this.label50);
            this.groupBox3.Controls.Add(this.label49);
            this.groupBox3.Controls.Add(this.numericUpDownBaseCompressibility);
            this.groupBox3.Controls.Add(this.label48);
            this.groupBox3.Controls.Add(this.numericUpDownFlowingCompressibility);
            this.groupBox3.Controls.Add(this.label47);
            this.groupBox3.Controls.Add(this.label46);
            this.groupBox3.Controls.Add(this.numericUpDownAtmosPressPSI);
            this.groupBox3.Controls.Add(this.label45);
            this.groupBox3.Controls.Add(this.numericUpDownAtmosPressKPA);
            this.groupBox3.Controls.Add(this.label35);
            this.groupBox3.Controls.Add(this.numericUpDownRefPress);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.numericUpDownSpecificGravity);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.numericUpDownRefTemp);
            this.groupBox3.Location = new System.Drawing.Point(21, 271);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(461, 366);
            this.groupBox3.TabIndex = 124;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Advanced Settings";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(389, 658);
            this.label60.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(74, 17);
            this.label60.TabIndex = 125;
            this.label60.Text = "E3M3/24H";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(591, 658);
            this.label61.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(66, 17);
            this.label61.TabIndex = 127;
            this.label61.Text = "MCF/24H";
            // 
            // numericUpDownFlowMCF
            // 
            this.numericUpDownFlowMCF.DecimalPlaces = 6;
            this.numericUpDownFlowMCF.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDownFlowMCF.Location = new System.Drawing.Point(487, 656);
            this.numericUpDownFlowMCF.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownFlowMCF.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.numericUpDownFlowMCF.Name = "numericUpDownFlowMCF";
            this.numericUpDownFlowMCF.ReadOnly = true;
            this.numericUpDownFlowMCF.Size = new System.Drawing.Size(96, 22);
            this.numericUpDownFlowMCF.TabIndex = 126;
            this.numericUpDownFlowMCF.ValueChanged += new System.EventHandler(this.numericUpDown3_ValueChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1188, 28);
            this.menuStrip1.TabIndex = 128;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveAsDefaultSettingsToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(74, 24);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // saveAsDefaultSettingsToolStripMenuItem
            // 
            this.saveAsDefaultSettingsToolStripMenuItem.Name = "saveAsDefaultSettingsToolStripMenuItem";
            this.saveAsDefaultSettingsToolStripMenuItem.Size = new System.Drawing.Size(239, 24);
            this.saveAsDefaultSettingsToolStripMenuItem.Text = "Save As Default Settings";
            this.saveAsDefaultSettingsToolStripMenuItem.Click += new System.EventHandler(this.saveAsDefaultSettingsToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1188, 694);
            this.Controls.Add(this.label61);
            this.Controls.Add(this.numericUpDownFlowMCF);
            this.Controls.Add(this.label60);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.checkBoxUseGasComp);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.numericUpDownResult);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.numericUpDownViscosity);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRefTemp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFlowingTemp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTubeDiameter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownOrificeDiameter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownViscosity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStaticPressure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDiffPressure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpecificGravity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMethane)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNitrogen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCarbonDioxide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownEthane)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPropane)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownWater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHydrogenSulfide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHydrogen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCarbonMonoxide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownOxygen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIButane)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNButane)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIPentane)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNPentane)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNHexane)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNHeptane)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNOctane)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNNonane)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNDecane)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHelium)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownArgon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRefPress)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDiffInH2O)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStaticPSI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownOrificeInch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPipeInch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTemperatureF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAtmosPressKPA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAtmosPressPSI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFlowingCompressibility)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBaseCompressibility)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBaseTempF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownOrificeTemperatureF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownOrificeTemperatureC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRefPressPSI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPipeTemperatureF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPipeTemperatureC)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFlowMCF)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBoxPressureTapUpstream;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDownRefTemp;
        private System.Windows.Forms.NumericUpDown numericUpDownFlowingTemp;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numericUpDownTubeDiameter;
        private System.Windows.Forms.NumericUpDown numericUpDownOrificeDiameter;
        private System.Windows.Forms.NumericUpDown numericUpDownViscosity;
        private System.Windows.Forms.NumericUpDown numericUpDownStaticPressure;
        private System.Windows.Forms.NumericUpDown numericUpDownDiffPressure;
        private System.Windows.Forms.NumericUpDown numericUpDownSpecificGravity;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numericUpDownResult;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.NumericUpDown numericUpDownMethane;
        private System.Windows.Forms.NumericUpDown numericUpDownNitrogen;
        private System.Windows.Forms.NumericUpDown numericUpDownCarbonDioxide;
        private System.Windows.Forms.NumericUpDown numericUpDownEthane;
        private System.Windows.Forms.NumericUpDown numericUpDownPropane;
        private System.Windows.Forms.NumericUpDown numericUpDownWater;
        private System.Windows.Forms.NumericUpDown numericUpDownHydrogenSulfide;
        private System.Windows.Forms.NumericUpDown numericUpDownHydrogen;
        private System.Windows.Forms.NumericUpDown numericUpDownCarbonMonoxide;
        private System.Windows.Forms.NumericUpDown numericUpDownOxygen;
        private System.Windows.Forms.NumericUpDown numericUpDownIButane;
        private System.Windows.Forms.NumericUpDown numericUpDownNButane;
        private System.Windows.Forms.NumericUpDown numericUpDownIPentane;
        private System.Windows.Forms.NumericUpDown numericUpDownNPentane;
        private System.Windows.Forms.NumericUpDown numericUpDownNHexane;
        private System.Windows.Forms.NumericUpDown numericUpDownNHeptane;
        private System.Windows.Forms.NumericUpDown numericUpDownNOctane;
        private System.Windows.Forms.NumericUpDown numericUpDownNNonane;
        private System.Windows.Forms.NumericUpDown numericUpDownNDecane;
        private System.Windows.Forms.NumericUpDown numericUpDownHelium;
        private System.Windows.Forms.NumericUpDown numericUpDownArgon;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown numericUpDownRefPress;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBoxUseGasComp;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.NumericUpDown numericUpDownDiffInH2O;
        private System.Windows.Forms.NumericUpDown numericUpDownStaticPSI;
        private System.Windows.Forms.NumericUpDown numericUpDownOrificeInch;
        private System.Windows.Forms.NumericUpDown numericUpDownPipeInch;
        private System.Windows.Forms.NumericUpDown numericUpDownTemperatureF;
        private System.Windows.Forms.NumericUpDown numericUpDownAtmosPressKPA;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.NumericUpDown numericUpDownAtmosPressPSI;
        private System.Windows.Forms.NumericUpDown numericUpDownFlowingCompressibility;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.NumericUpDown numericUpDownBaseCompressibility;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.NumericUpDown numericUpDownBaseTempF;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.NumericUpDown numericUpDownOrificeTemperatureF;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.NumericUpDown numericUpDownOrificeTemperatureC;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numericUpDownRefPressPSI;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.NumericUpDown numericUpDownPipeTemperatureF;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.NumericUpDown numericUpDownPipeTemperatureC;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboBoxOrificeMaterial;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.ComboBox comboBoxPipeMaterial;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.NumericUpDown numericUpDownFlowMCF;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsDefaultSettingsToolStripMenuItem;
    }
}

