def BarToMillibar(bar):
    return bar * 1000.0


def MillibarToBar(millibar):
    if millibar == 0:
        return 0
    return millibar / 1000.0


def BarToKPA(bar):
    return bar * 100.0


def KPAToBar(kpa):
    if kpa == 0:
        return 0
    return kpa / 100.0


def ft3Tom3(ft3):
    return ft3 / 35.315


def MillibarToKPA(millibar):
    if millibar == 0:
        return 0
    return millibar / 10.0


def KPAToMillibar(kpa):
    return kpa * 10.0


def inH2OToPSI(inH20):
    return inH20 * 0.03612728691


def PSIToinH2O(psi):
    return psi / 0.03612728691


def MCFtoE3M3(mcf):
    return mcf * 0.028174


def E3M3toMFC(e3m3):
    if e3m3 == 0:
        return 0
    return e3m3 / 0.028174


def CELCIUStoFARHENHEIT(celsius):
    return (celsius * 9.0 / 5.0) + 32.0


def FARHENHEITtoCELCIUS(farhenheit):
    return (farhenheit - 32.0) * 0.55556
        
        
def CELCIUStoKELVIN(celsius):
    return celsius + 273.15


def InH2OtoKPA(inH2O):
    if inH2O == 0:
        return 0
    return inH2O * 0.249088908


def KPAtoINH2O(kpa):
    if kpa == 0:
        return 0
    return kpa * 4.014630786
  

def PSItoKPA(psi):
    return psi * 6.8948


def KPAtoPSI(kpa):
    if kpa == 0:
        return 0
    return kpa / 6.8948


def INCHtoMM(inch):
    return inch * 25.4
           

def MMtoINCH(mm):
    return mm * 0.0393700787402


def E3M3toMMCF(e3m3):
    return e3m3 * 0.035494
