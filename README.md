# AGA Gas Flow Calculator(AGA3 & AGA8)

Reference: http://www.scadacore.com/field-applications/aga-calculators/aga-gas-flow-calculator/

## Install dependencies
    
### Install kivy
    
    sudo easy_install --upgrade pip
    
    sudo apt-get install libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev \ 
    libsdl2-ttf-dev pkg-config libgl1-mesa-dev libgles2-mesa-dev \ 
    python-setuptools libgstreamer1.0-dev git-core \
    gstreamer1.0-plugins-{bad,base,good,ugly} \
    gstreamer1.0-{omx,alsa} python-dev
    
    sudo pip install Cython==0.23
    
    sudo pip install git+https://github.com/kivy/kivy.git@master
    
### Install some other packages.
    
    sudo pip install modbus_tk
    